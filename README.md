### Snapshot Updater

This project is a tiny app to help quickly update snapshots in bulk. 

![screenshot](screenshot.png)

### Installation
1. Clone this repo
2. Open products/SnapshotUpdater.app

### How to Use

The workflow goes something like this 

1. Realize the primary text color is off and slighly innacessible
2. Fix the text color and push up your branch
3. CI yells at you for having 300 failing snapshots
4. You click the failing build and choose Job Artifacts > Download in the right hand side inspector
5. You unzip artifacts.zip
6. Open up SnapshotUpdater and choose the artifacts zip.
7. You can now view all your failing snapshots and hover over them to see what changed.
8. Snapshots that changed as expected (eg. just a slightly jiggle or text color change) you click and mark as "looks good"
9. You hit Update Snapshot and direct the file picker to the project root of "iosapp"
10. Snapshot Updater will take all your approved failure images and make them the new reference pictures 



### Warning
This was built in an L&D day (and change) so is fairly hacked together. Bugs and Errors are likely, make sure you note down the filenames of any errors surfaced and checkout those snapshots yourself.

Also man, the code is hacked together, no judgement pls
