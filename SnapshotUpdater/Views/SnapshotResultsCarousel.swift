//
//  SnapshotResultsCarousel.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import SwiftUI

struct SnapshotResultsCarousel: View {
    
    @EnvironmentObject
    var viewModel: SnapshotUpdaterViewModel
    
    var results: [SnapshotResult]
    
    var body: some View {
        ScrollView(.horizontal) {
            HStack {
                ForEach(results) { result in
                    let isHovering = viewModel.hoveredId == result.id
                    let isOkayed = viewModel.okayedSnapshotIds.contains(result.id)
                    let resolvedImage: SnapshotImage = isOkayed || isHovering ? result.failing : result.reference
                    
                    ZStack {
                        VStack(spacing: 8) {
                            Image(nsImage: resolvedImage.image)
                                .resizable()
                                .scaledToFit()
                            
                            Text(resolvedImage.snapshotType.displayTitle)
                        }
                        .padding()
                        
                        if isOkayed {
                            looksGoodOverlay(showingPill: true)
                        }
                        else if isHovering {
                            looksGoodOverlay(showingPill: false)
                                .opacity(0.3)
                        }
                        if isHovering {
                            HStack {
                                VStack {
                                    Button {
                                        viewModel.ksdiffThisShit(for: result)
                                    } label: {
                                        Image(systemName: "k.circle")
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 24, height: 24)
                                            .foregroundColor(Color.accentColor)
                                    }
                                    .buttonStyle(PlainButtonStyle())
                                    Spacer()
                                }.padding()

                                Spacer()
                                
                                VStack {
                                    Button {
                                        viewModel.openAllUrls(for: result)
                                    } label: {
                                        Image(systemName: "doc")
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 24, height: 24)
                                            .foregroundColor(Color.accentColor)
                                    }
                                    .buttonStyle(PlainButtonStyle())
                                    Spacer()
                                }.padding()
                            }

                        }
                    }
                    .frame(width: 400, height: 1200)
                    .background(
                        Color.white.opacity(0.2)
                            .cornerRadius(8)
                    )
                    .onTapGesture {
                        viewModel.tap(result: result)
                    }
                    .onHover { hovering in
                        viewModel.hoveredId = hovering ? result.id : nil
                    }
                        
                }
            }
            .padding()
        }
        .background(Color.gray.opacity(0.15))
    }
    
    func looksGoodOverlay(showingPill: Bool) -> some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                if showingPill {
                    ImageOverlayPill(text: "Looks Good!", color: .green)
                }
            }
        }
        .padding()
        .border(Color.green, width: 4)
        .cornerRadius(8)
    }
}
