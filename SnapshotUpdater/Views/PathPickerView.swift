//
//  PathPickerView.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import SwiftUI

struct PathPickerView: View {
    @EnvironmentObject
    var viewModel: SnapshotUpdaterViewModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
//            HStack {
//                TextField("Source", text: .constant(viewModel.sourcePath?.absoluteString ?? ""))
                Button("Choose Artifacts", action: viewModel.tappedChooseSource)
//            }
//            HStack {
//                TextField("Dest", text: .constant(viewModel.destinationPath?.absoluteString ?? ""))
//                Button("Choose", action: viewModel.tappedChooseDestination)
//            }
        }.padding()
    }
}
