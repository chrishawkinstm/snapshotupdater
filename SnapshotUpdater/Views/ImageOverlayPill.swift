//
//  ImageOverlayPill.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import SwiftUI

struct ImageOverlayPill: View {
    var text: String
    var color: Color
    
    var body: some View {
        HStack {
            Text(text)
                .foregroundColor(.white)
            
        }
        .padding(4)
        .background(color)
        .clipShape(RoundedRectangle(cornerRadius: 10))

    }
}
