//
//  SetupView.swift
//  SnapshotUpdater
//
//  Created by chawkins on 19/07/21.
//

import SwiftUI

struct SetupView: View {
    
    @EnvironmentObject
    var viewModel: SnapshotUpdaterViewModel
    
    private var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, h:mm a"
        return formatter
    }()
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Searching for artifacts in ")
                
                Button(self.viewModel.possibleArtifactSearchUrl.lastPathComponent.lowercased()) {
                    self.viewModel.selectedDownloadSearchFolder()
                }
                .buttonStyle(PlainButtonStyle())
                .foregroundColor(.accentColor)
            }
            
            HStack(spacing: 16) {
                VStack(alignment: .leading) {
                    
                    List(viewModel.possibleArtifactUrls, id:\.self, selection: $viewModel.selectedPossibleArtifactUrl) { possibility in
                        HStack {
                            HStack {
                                Text(possibility.name)
                                Spacer()
                            }
                            .frame(width: 120)
                            
                            Text(dateFormatter.string(from: possibility.lastModified))
                                .foregroundColor(.secondary)
                        }
                        .onTapGesture(count: 2) {
                            viewModel.tryLoadPossibleArtifactsUrl()
                        }
                    }
                    .listStyle(InsetListStyle())
                    .cornerRadius(8)
                }
                .frame(minWidth: 300)
                
                VStack {
                    Text("Snapshot Updater will walk you through all your failing snapshots and help you quickly update those that are correct.\n\nTo get started download the \"Job Artifacts\" from the failing build on GitLab and unzip it.")
                    Spacer()
                    HStack {
                        Spacer()
                        Button("Load Artifacts", action: viewModel.tryLoadPossibleArtifactsUrl)
                            .disabled(viewModel.selectedPossibleArtifactUrl == nil)
                    }
                }
                .frame(width: 250)
            }
        }
        .padding()
        .toolbar(content: {
            Button(action: viewModel.tappedRefresh, label: {
                Image(systemName: "arrow.clockwise")
            })
        })
    }
}
