//
//  ResultsView.swift
//  SnapshotUpdater
//
//  Created by chawkins on 19/07/21.
//

import SwiftUI

struct ResultsView: View {

    @EnvironmentObject
    var viewModel: SnapshotUpdaterViewModel

    var resultGroups: [SnapshotTestGroupResult]
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                VStack {
                    HStack {
                        Spacer()
                        Button("Toggle Okay Current", action: viewModel.toggleOkayAllInSelectedGroup)
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        Button("Toggle Okay All", action: viewModel.toggleOkayAll)
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        Button("Update Snapshots", action: viewModel.tapMove)
                        Spacer()
                    }
                }
                
                Spacer()
                
                List(resultGroups, id:\.self, selection: $viewModel.selectedResultGroup) { group in
                    Text(group.title)
                }
                .frame(minWidth: 250)
                .listStyle(SidebarListStyle())
                .navigationTitle("Hello")
                .toolbar {
                    Button(action: viewModel.selectedBack, label: {
                        Image(systemName: "chevron.backward")
                    })
                    Button(action: toggleSidebar, label: {
                        Image(systemName: "sidebar.left")
                    })
                    Button(action: viewModel.tappedResultsRefresh, label: {
                        Image(systemName: "arrow.clockwise")
                    })
                }
                .padding()

            }
            ScrollView(.vertical) {
                if let selectedGroup = viewModel.selectedResultGroup {
                    SnapshotResultsCarousel(results: selectedGroup.results)
                        .frame(minWidth: 1200, minHeight: 1200)
                }
            }
        }
    }
    
    private func toggleSidebar() {
        NSApp.keyWindow?.firstResponder?.tryToPerform(#selector(NSSplitViewController.toggleSidebar(_:)), with: nil)
    }
}
