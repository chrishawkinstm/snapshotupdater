//
//  ContentView.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject
    var viewModel: SnapshotUpdaterViewModel
    
    var body: some View {
        VStack {
            if let resultGroups = viewModel.resultGroups {
                ResultsView(resultGroups: resultGroups)
            }
            else {
                SetupView()
            }
        }
        .alert(item: $viewModel.alertMessage, content: { alert in
            switch alert {
            case .error(let message):
                return Alert(
                    title: Text("You messed up, you manus!"),
                    message: Text(message),
                    dismissButton: .default(Text("You're a manus"))
                )
            case .success(let message):
                return Alert(
                    title: Text("Good job, ya manus!"),
                    message: Text(message),
                    dismissButton: .default(Text("Stop calling me a manus"))
                )
            }
        })
        .onAppear(perform: viewModel.appear)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(SnapshotUpdaterViewModel())
    }
}

extension String: Identifiable {
    public var id: String { self }
}

struct VerticalLabelStyle: LabelStyle {
    func makeBody(configuration: Configuration) -> some View {
        VStack {
            configuration.icon.font(.headline)
            configuration.title.font(.subheadline)
        }
    }
}
