//
//  SnapshotImage.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import AppKit

struct SnapshotImage: Hashable {
    enum SnapshotImageType {
        case failing
        case reference
        case diff
        
        var displayTitle: String {
            switch self {
            case .failing:
                return "Failing (new)"
            case .reference:
                return "Reference (old)"
            case .diff:
                return "Diff"
            }
        }
        
    }
    
    var url: URL
    var image: NSImage
    var snapshotType: SnapshotImageType
    
    init(url: URL) {
        self.url = url
        self.image = NSImage(contentsOf: url)!
        if url.absoluteString.contains("failing") {
            snapshotType = .failing
        }
        else if url.absoluteString.contains("reference") {
            snapshotType = .reference
        }
        else {
            snapshotType = .diff
        }
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(url)
    }
    
    static func ==(lhs: SnapshotImage, rhs: SnapshotImage) -> Bool {
        return lhs.url == rhs.url
    }
}
