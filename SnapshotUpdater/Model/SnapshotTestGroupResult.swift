//
//  SnapshotTestGroupResult.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import SwiftUI


struct SnapshotTestGroupResult: Identifiable, Hashable {
    var title: String
    var results: [SnapshotResult]
    var isExpanded: Bool = false
    
    var id: String {
        title
    }
}
