//
//  SnapshotResult.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import AppKit

struct SnapshotResult: Identifiable, Hashable {
    var failing: SnapshotImage
    var reference: SnapshotImage
    var diff: SnapshotImage
    
    init(failingUrl: URL, referenceUrl: URL, diffUrl: URL) {
        self.failing = .init(url: failingUrl)
        self.reference = .init(url: referenceUrl)
        self.diff = .init(url: diffUrl)
    }
    
    var id: String {
        return failing.url.absoluteString
    }
    
    var baseFileName: String {
        failing.url.lastPathComponent.replacingOccurrences(of: "failing-", with: "")
    }
}
