//
//  SnapshotUpdaterApp.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import SwiftUI

@main
struct SnapshotUpdaterApp: App {
    @State
    var viewModel: SnapshotUpdaterViewModel = SnapshotUpdaterViewModel()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(viewModel)
        }
    }
}
