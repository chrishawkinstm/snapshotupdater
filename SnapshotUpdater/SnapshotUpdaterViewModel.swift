//
//  SnapshotUpdaterViewModel.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import SwiftUI

struct PossibleArtifactSource: Identifiable, Hashable {
    let url: URL
    let name: String
    let lastModified: Date
    
    init(url: URL) {
        self.url = url
        self.name = FileManager.default.displayName(atPath: url.path)
        self.lastModified = try! url.resourceValues(forKeys: [.contentModificationDateKey]).contentModificationDate!
    }
    
    var id: String {
        name
    }
}

class SnapshotUpdaterViewModel: ObservableObject {
    
    @Published
    var snapshotResults: [SnapshotResult]?
    
    @Published
    var resultGroups: [SnapshotTestGroupResult]? {
        didSet {
            selectedResultGroup = resultGroups?.first
        }
    }
    
    @Published
    var okayedSnapshotIds: [String] = []
    
    @Published
    var sourcePath: URL?
    
    @Published
    var failureDiffsPath: URL?

    @Published
    var destinationPath: URL?
    
    @AppStorage("lastSelectedDestinationPathString")
    var lastSelectedDestinationPathString: String = ""

    @Published
    var hoveredId: String?
    
    @Published
    var selectedResultGroup: SnapshotTestGroupResult?
    
    enum AlertMessage: Identifiable {
        var id: String {
            switch self {
            case .error(let message), .success(let message):
                return message
            }
        }
        case error(String), success(String)
    }
    
    @Published
    var alertMessage: AlertMessage?

    @AppStorage("possibleArtifactSearchUrl")
    var possibleArtifactSearchUrl: URL = try! FileManager.default.url(for: .downloadsDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    
    @Published
    var possibleArtifactUrls: [PossibleArtifactSource] = []
    
    @Published
    var selectedPossibleArtifactUrl: PossibleArtifactSource?
    
    private func toggleOkayedStatus(of result: SnapshotResult) {
        if okayedSnapshotIds.contains(result.id) {
            okayedSnapshotIds.removeAll(where: { $0 == result.id })
        }
        else {
            okayedSnapshotIds.append(result.id)
        }
    }
    
    func appear() {
        searchForArtifacts()
    }
    
    func tappedRefresh() {
        self.possibleArtifactUrls = []
        self.snapshotResults = []
        self.selectedResultGroup = nil
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.searchForArtifacts()
        }
    }
    
    func tappedResultsRefresh() {
        self.resultGroups = []
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tryLoadPossibleArtifactsUrl()
        }
    }
    
    func selectedBack() {
        self.failureDiffsPath = nil
        self.resultGroups = nil
    }
    
    func searchForArtifacts() {
        let enumerator = FileManager.default.enumerator(at: self.possibleArtifactSearchUrl, includingPropertiesForKeys: [.nameKey, .contentModificationDateKey], options: [.skipsSubdirectoryDescendants], errorHandler: nil)!
        
        self.possibleArtifactUrls = enumerator
            .compactMap { $0 as? URL }
            .filter { $0.lastPathComponent.contains("output") }
            .map { PossibleArtifactSource(url: $0) }
            .sorted(by: { $0.lastModified < $1.lastModified })
            .reversed()
    }
    
    func selectedDownloadSearchFolder() {
        NSOpenPanel.displayDirectoryPicker { url in
            self.possibleArtifactSearchUrl = url
            self.searchForArtifacts()
        }
    }

    func toggleOkayAll() {
        if !okayedSnapshotIds.isEmpty {
            okayedSnapshotIds = []
        } else {
            let allResults = resultGroups?.flatMap(\.results).map(\.id) ?? []
            okayedSnapshotIds = allResults
        }
    }

    func toggleOkayAllInSelectedGroup() {
        guard let group = selectedResultGroup else { return }

        let groupResultIds = group.results.map(\.id)
        let isNothingOkayed = Set(okayedSnapshotIds).isDisjoint(with: groupResultIds)

        var previouslyOkayedMinusSelectedGroup = okayedSnapshotIds
        previouslyOkayedMinusSelectedGroup.removeAll(where: groupResultIds.contains)

        if isNothingOkayed {
            okayedSnapshotIds = previouslyOkayedMinusSelectedGroup + groupResultIds // okay everything selected
        } else {
            okayedSnapshotIds = previouslyOkayedMinusSelectedGroup // unokay selected
        }
    }
    
    func tap(result: SnapshotResult) {
        toggleOkayedStatus(of: result)
    }
    
    func tryLoadPossibleArtifactsUrl() {
        guard let selectedPossibleArtifactUrl = selectedPossibleArtifactUrl else { return }
        self.failureDiffsPath = selectedPossibleArtifactUrl.url
            .appendingPathComponent("FailureDiffs", isDirectory: true)
        self.loadFolders()
    }
    
    func loadTestGroup(path: URL) -> SnapshotTestGroupResult {
        do {
            let files = try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: [.nameKey], options: [])
            let failingUrls = files.filter { $0.absoluteString.contains("failing") }
            let referenceUrls = files.filter { $0.absoluteString.contains("reference") }
            let diffUrls = files.filter { $0.absoluteString.contains("diff") }
            
            let snapshotResults: [SnapshotResult] = failingUrls.compactMap { failingUrl in
                let baseFileName = failingUrl.lastPathComponent.replacingOccurrences(of: "failing-", with: "")
                guard let diffUrl = diffUrls.first(where: { $0.absoluteString.contains(baseFileName) }) else { return nil }
                guard let referenceUrl = referenceUrls.first(where: { $0.absoluteString.contains(baseFileName) }) else { return nil }
                return SnapshotResult(failingUrl: failingUrl, referenceUrl: referenceUrl, diffUrl: diffUrl)
            }
            let folderName = path.lastPathComponent
            return .init(title: folderName, results: snapshotResults)
        }
        catch {
            print(error)
            fatalError()
        }
    }
    
    func openAllUrls(for result: SnapshotResult) {
        let config = NSWorkspace.OpenConfiguration()
        
        NSWorkspace.shared.open([
            result.failing.url,
            result.reference.url,
            result.diff.url,
        ], withApplicationAt: URL(fileURLWithPath: "/System/Applications/Preview.app"), configuration: config) { application, error in

        }
    }

    func ksdiffThisShit(for result: SnapshotResult) {
        do {
            try Process.run(URL(fileURLWithPath: "/usr/local/bin/ksdiff"),
                            arguments: [result.failing.url, result.reference.url].map(\.relativePath))
        } catch {
            print(error)
        }
    }
    
    func tapMove() {
        NSOpenPanel.displayDirectoryPicker { url in
            self.destinationPath = url
            guard let resultGroups = self.resultGroups else { return }
            guard let baseDestinationPath = self.destinationPath else { return }

            let resourceKeys = Set<URLResourceKey>([.nameKey, .isDirectoryKey])
            let testNames = Set(resultGroups.map(\.title))
            do {
                let pathEnumerator = FileManager.default.enumerator(at: baseDestinationPath,
                                                                    includingPropertiesForKeys: Array(resourceKeys),
                                                                    options: .skipsHiddenFiles
                )!
                var copiedSnapshots: Set<String> = .init()
                for case let groupDestinationPath as URL in pathEnumerator {

                    guard let resourceValues = try? groupDestinationPath.resourceValues(forKeys: resourceKeys),
                          let isDirectory = resourceValues.isDirectory,
                          let name = resourceValues.name
                    else {
                        continue
                    }

                    if isDirectory {
                        if ["xcodeproj", "xcassets", "Pods"].contains(where: name.contains) {
                            pathEnumerator.skipDescendants()
                            continue
                        }
                    } else {
                        continue
                    }

                    let testName = groupDestinationPath.lastPathComponent
                    if testNames.contains(testName) {
                        guard let resultGroup = resultGroups.first(where: { $0.title == testName }) else { continue }
                        for result in resultGroup.results {
                            guard self.okayedSnapshotIds.contains(result.id) else { continue }
                            let fileDestinationPath = groupDestinationPath.appendingPathComponent(result.baseFileName, isDirectory: false)
                            if FileManager.default.fileExists(atPath: fileDestinationPath.path) {
                                try FileManager.default.removeItem(at: fileDestinationPath)
                            }
                            try FileManager.default.copyItem(at: result.failing.url, to: fileDestinationPath)
                            copiedSnapshots.insert(result.baseFileName)
                        }
                    }
                }
                self.alertMessage = .success("Successfully updated \(copiedSnapshots.count) snapshots")
            }
            catch {
                self.alertMessage = .error(error.localizedDescription)
            }
        }
    }
    
    func tappedChooseSource() {
        NSOpenPanel.displayDirectoryPicker { url in
            self.failureDiffsPath = url
            self.loadFolders()
        }
    }
    
    func loadFolders() {
        guard let failureDiffsPath = self.failureDiffsPath else { return }
        do {
            let folders = try FileManager.default.contentsOfDirectory(at: failureDiffsPath, includingPropertiesForKeys: [.nameKey], options: [.skipsHiddenFiles])
            self.resultGroups = folders.map(loadTestGroup(path:)).sorted(by: { result1, result2 in
                result1.title < result2.title
            })
        }
        catch {
            print(error)
        }
    }
    
    func tappedChooseDestination() {
        NSOpenPanel.displayDirectoryPicker(startingUrl: URL(string: lastSelectedDestinationPathString)) { url in
            self.lastSelectedDestinationPathString = url.absoluteString
            self.destinationPath = url
        }
    }
    
    func toggleExpansion(of group: SnapshotTestGroupResult) {
        guard let index = resultGroups?.firstIndex(where: { $0.id == group.id }) else { return }
        resultGroups?[index].isExpanded.toggle()
    }
}
