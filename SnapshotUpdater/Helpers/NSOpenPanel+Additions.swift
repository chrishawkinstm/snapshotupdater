//
//  NSOpenPanel+Additions.swift
//  SnapshotUpdater
//
//  Created by chawkins on 16/07/21.
//

import AppKit

extension NSOpenPanel {
    
    static func displayDirectoryPicker(startingUrl: URL? = nil, then completion: @escaping (URL) -> Void) {
        let panel = NSOpenPanel()
        panel.directoryURL = startingUrl
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = true
        panel.canChooseFiles = false
        panel.begin { response in
            guard response == .OK else { return }
            guard let url = panel.url else { return }
            completion(url)
        }
    }
}
